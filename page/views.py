from rest_framework import viewsets, mixins
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route
from django.core.cache import cache

from .models import User, Dummy
from explore.models import PageFrequency as PF, PageDate as PD, StreamUser
from .serializers import KeywordSerializer
from mongoengine.queryset.visitor import Q
import django_rq


class UserViewSet(viewsets.GenericViewSet):
    queryset = Dummy.objects.all()

    action_serializers = {
        'keyword': KeywordSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    # def list(self, request):
    #     from random import randint
    #     user_id = request.GET.get('user', '')
    #     keyword = request.GET.get('keyword', '')
    #
    #     user = User.objects(id=user_id).first()
    #     # pf_list = PF.objects(user=user)[:int(key)]
    #     response = []
    #     start = "A"
    #     for i in range(0, int(keyword)):
    #         response.append({
    #             "value": chr(ord(start) + i),
    #             "count": randint(1,20)
    #         })
    #     return Response(response)

    @list_route(methods=['get'], url_path='stream')
    def stream(self, request, pk=None):
        user_id = request.GET.get('user_id', '')
        date_before = request.GET.get('before', '')
        date_after = request.GET.get('after', '')
        mode = cache.get("mode")
        word_list = StreamUser.objects(user_id=user_id,
                                date_before=date_before,
                                date_after=date_after,
                                types=mode).first().word_list

        user = User.objects(id=user_id).first()
        date_list = PD.objects(Q(date__gte=date_before) &
                               Q(date__lte=date_after)).order_by('date')
        response = []
        for i, date in enumerate(date_list):
            use_word_list = dict(word_list)
            if mode == 1:
                pf_list = list(PF.objects(user=user, date=date, category__startswith="T"))
            else:
                pf_list = list(PF.objects(user=user, date=date))

            for pf in pf_list:
                if pf.top_keyword:
                    for word in pf.top_keyword:
                        if word['word'] in use_word_list.keys():
                            use_word_list[word['word']] += word['weight']

            for word in list(use_word_list.keys()):
                response.append({
                    "key": word,
                    "value": round(use_word_list[word]),
                    "date": date.date.date()
                })
        return Response(response)


    @list_route(methods=['post'], url_path='keyword')
    def keyword(self, request, pk=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        mode = cache.get("mode")

        from operator import itemgetter
        user = User.objects(id=serializer.data['user_id']).first()

        date_list = PD.objects(Q(date__gte=serializer.data['before']) &
                               Q(date__lte=serializer.data['after']))
        keyword_list = []
        word_list = {}
        if mode == 1:
            pf_list = list(PF.objects(user=user, date__in=date_list, category__startswith="T"))
        else:
            pf_list = list(PF.objects(user=user, date__in=date_list))
        for pf in pf_list:
            if pf.top_keyword:
                for word in pf.top_keyword:
                    if word['word'] not in word_list:
                        word_list.update({
                            word['word']: word['weight'],
                        })
                    else:
                        word_list[word['word']] += word['weight']

        for word in word_list.keys():
            keyword_list.append({
                "value": word,
                "count": round(word_list[word])
            })

        keyword_list = sorted(keyword_list, key=itemgetter('count'), reverse=True)[:100]

        temp = StreamUser.objects(user_id=serializer.data['user_id'],
                                  date_before=serializer.data['before'],
                                  date_after=serializer.data['after'],
                                  types=mode).first()
        if not temp:
            stream = keyword_list[:10]
            word_list = {}
            for word in stream:
                word_list.update({word['value']: 0})
            StreamUser(user_id=serializer.data['user_id'],
                       date_before=serializer.data['before'],
                       date_after=serializer.data['after'],
                       word_list=word_list,
                       types=mode).save()

        return Response(keyword_list)


class PageViewSet(mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    queryset = Dummy.objects.all()

    def list(self, request):
        return Response([])

    # def list(self, request):
    #     queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=31536000)
    #     for i in range(1, 9):
    #         queue.enqueue(make_data, str(i))
    #     return Response(status=status.HTTP_200_OK)

    # @list_route(methods=['get'], url_path='get-raw-html')
    # def get_url(self, request):
    #     queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=31536000)
    #     for index, user in enumerate(list(User.objects[100:103])):
    #         queue.enqueue(get_raw_html, [index+101, user])
    #     return Response(status=status.HTTP_200_OK)
    #
    # @list_route(methods=['get'], url_path='clean-ip')
    # def clean_ip(self, request):
    #     queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=31536000)
    #     queue.enqueue(clean)
    #     return Response(status=status.HTTP_200_OK)

    # @list_route(methods=['get'], url_path='remove-dup')
    # def remove_dup(self, request):
    #     queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=31536000)
    #     for i, user in enumerate(User.objects):
    #         queue.enqueue(remove, [i, user], timeout=31536000)
    #     return Response(status=status.HTTP_200_OK)

    # @list_route(methods=['get'], url_path='get-domain')
    # def get_url_domain(self, request):
    #     queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=31536000)
    #     queue.enqueue(get_domain)
    #     return Response(status=status.HTTP_200_OK)


def clean():
    from time import time
    from core.extract_keywords import is_all_ip
    i = 5000001
    start_time = time()
    page_list = Page.objects[5000000:]
    print("query !!")
    for page in page_list:
        if is_all_ip(page.domain):
            print("deleted", page.url)
            page.delete()
        if i%100000 == 0:
            print(i, "passed, use", (time()-start_time))
        i += 1

def get_domain():
    from time import time
    i = 1
    start_time = time()
    for page in Page.objects[6463653:]:
        try:
            page.domain = page.url.split("/")[2]
            page.save()
        except:
            print(page.url)
            page.delete()
        if i%100000 == 0:
            print(i, "passed, use", (time()-start_time))
        i += 1

def remove(inputs):
    print("user", inputs[0]+1, "start")
    from time import time
    start_time = time()
    page_user = Page.objects(user=inputs[1])
    url_group = page_user.aggregate({"$group": {"_id": "$url"}})
    for url in list(url_group):
        page_list = page_user(url=url["_id"])
        if page_list.count() == 1:
            page = page_list.first()
            page.history.append({
                "time_access": page.time_access,
                "time_taken": page.time_taken
            })
            page.save()
        else:
            active = page_list.first()
            remain = page_list[1:]
            active.history.append({
                "time_access": active.time_access,
                "time_taken": active.time_taken
            })
            for page in remain:
                active.history.append({
                    "time_access": page.time_access,
                    "time_taken": page.time_taken
                })
            active.save()
            remain.delete()
    description = "duplicate user " + str(inputs[0]+1) + " removed"
    Time(description=description, time=(time()-start_time)).save()
    print(inputs[0]+1, "success")

def get_raw_html(inputs):
    import os
    from datetime import datetime
    from core.extract_keywords import get_raw
    from time import time

    index = inputs[0]
    user = inputs[1]
    temp_page = Page(url="test", ip="", time_taken=0, raw="", time_access=datetime.now())
    temp_page.save()
    delete = temp_page
    start_time = time()

    print(index, "start !!")
    page_list = list(Page.objects(user=user))
    print("get raw html: user", str(index), "query page")
    i = 1
    for page in page_list:
        if page.url == temp_page.url:
            page.raw = temp_page.raw
        else:
            page.raw = get_raw(page.url)
            temp_page = page
        page.save()
        if i%10000 == 0:
            print(i, "passed, use", (time()-start_time))
        i += 1
    description = "get raw html: user " + str(index)
    print(description)
    Time(description=description, time=(time()-start_time)).save()
    delete.delete()

def make_data(i):
    print(i, "start")
    import os
    import pandas as pd
    from datetime import datetime
    from time import time
    start_time = time()
    form = "%Y-%m-%d %H:%M:%S.%f"
    user = User(name="test")
    read_chunk = []
    for chunk in pd.read_csv(os.path.join(os.getcwd(), 'core/data/new_' + i + '.csv'), sep=',', chunksize=20000):
        read_chunk.append(chunk)

    page_list = pd.concat(read_chunk, axis=0)
    print("imported new_", i, ".csv")
    for index, page in page_list.iterrows():
        username = page['cs_username']
        if user.name != username:
            user = User.objects(name=username).first()
            if not user:
                user = User(name=username)
                user.save()

        Page(user=user,
             ip=page['c_ip'],
             url=page['cs_referer'],
             time_access=datetime.strptime(page['eventtime'], form),
             time_taken=page['time_taken']).save()
    description = "import new_" + str(i) + ".csv"
    Time(description=description, time=(time()-start_time)).save()
    print(i, "success")
