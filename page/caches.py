from django.core.cache import cache
from page.models import User
# from .models import PageDate as PD, PageFrequency as PF, Corpus, TopKeyword as TK

def get_time_out_day():
    return 24 * 60 * 60

def cached_train_user():
    key = 'train_user'
    result = cache.get(key)

    if result is None:
        try:
            user_all = User.objects
            result = []
            for i in [21, 51, 53, 59, 61, 87, 91, 92, 95, 97]:
                result.append(user_all[i])
            cache.set(key, result, get_time_out_day())
        except User.DoesNotExist:
            result = -1
    return None if result == -1 else result
