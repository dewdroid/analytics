from rest_framework import serializers


class KeywordSerializer(serializers.Serializer):

    user_id = serializers.CharField(required=True)
    before = serializers.CharField(required=True)
    after = serializers.CharField(required=True)
