from mongoengine import *
from django.db import models


class Dummy(models.Model):
    dummy = models.IntegerField(default=0)


class Time(Document):
    description = StringField(required=True)
    time = FloatField(required=True)


class User(Document):
    name = StringField(required=True)
    show_name = StringField(null=True)

    @staticmethod
    def pull_train():
        from .caches import cached_train_user
        return cached_train_user()


class Page(Document):
    user = ReferenceField(User)
    ip = StringField(required=True, null=True)
    url = StringField(required=True)
    domain = StringField(null=True)
    history = ListField(DictField(), default=list)
    time_access = DateTimeField(required=True)
    time_taken = IntField(required=True)
    category = StringField(null=True)
    raw = StringField(null=True)
    keyword = ListField(DictField(), default=list)

    def __str__(self):
        ip = self.ip + " | " if self.ip else ""
        url = self.url if self.url else ""
        return ip + url
