import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'project.settings'
django.setup()
from explore.models import PageFrequency as PF, PageDate as PD
from page.models import User, Page
import pandas as pd
import numpy as np
from mongoengine import *
connect('analytics')

import django_rq
worker = django_rq.get_worker()
worker.work()
