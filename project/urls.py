from django.urls import path
from django.conf.urls import include
from rest_framework_swagger.views import get_swagger_view
from rest_framework.routers import DefaultRouter

from page.views import PageViewSet, UserViewSet
from explore.views import PFViewSet, PDViewSet, KeywordViewSet


schema_view = get_swagger_view(title='Analytics API')
router = DefaultRouter()

# router.register(r'page', PageViewSet)
router.register(r'date', PDViewSet)
router.register(r'page', PFViewSet)
router.register(r'user', UserViewSet)
router.register(r'keyword', KeywordViewSet)

urlpatterns = [
    path('', schema_view),
    path('django-rq/', include('django_rq.urls')),
    path('', include(router.urls)),
]
