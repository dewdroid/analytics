from bs4 import BeautifulSoup
import urllib3
import os
urllib3.disable_warnings()

http = urllib3.PoolManager()
try:
    html = http.request('GET', "http://bigdataexperience.org/technological-trends-big-data-analytics", timeout=5.0)
except:
    print("error: timeout")
try:
    raw_html = html.data.decode('utf-8')
except:
    raw_html = html.data
raw_data = BeautifulSoup(raw_html, 'html.parser')
for script in raw_data(['script', 'style', '[document]']):
    script.extract()

save_path = os.path.join(os.getcwd(), "test.html")
with open(save_path, 'w') as destination:
    destination.write(str(raw_data))
