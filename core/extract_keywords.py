def isEnglish(s):
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True


def is_all_num(word):
    return all(ord(i) >= 48 and ord(i) <= 57 for i in word)


def is_all_thai(word):
    return all((ord(i) >= 3585 and ord(i) <= 3642) or \
               (ord(i) >= 3647 and ord(i) <= 3673) for i in word)


def is_all_ip(word):
    return all((ord(i) == 46) or \
               (ord(i) == 58) or \
               (ord(i) >= 48 and ord(i) <= 57) \
                for i in word)


def get_domain(url):
    del_pre = url.split('//', 1)[1]
    del_suf = del_pre.split('/', 1)[0]
    if 'www.' in del_suf:
        return del_suf.split['.', 1]
    return del_suf


def tokenize_word(text):
    from nltk import word_tokenize
    from nltk.stem.wordnet import WordNetLemmatizer
    from .stopwords import stopwords, thai_stopwords
    from deepcut import tokenize

    lmtzr = WordNetLemmatizer()
    trash = "'}{][)(,.\/|$%^&*@#!-–+`~_;:=<>? "
    trash_1 = '"'
    speech = ['n', 'v', 'a', 'r']
    word_list = []
    for word in tokenize(text):
        if isEnglish(word) and not is_all_num(word):
            is_append = False
            word_low = word.lower()
            for _type in speech:
                temp = lmtzr.lemmatize(word_low, _type)
                if temp != word_low:
                    is_append = True
                    word_list.append(temp)
                    break
            if not is_append:
                word_list.append(word_low)
        elif is_all_thai(word):
            if word not in thai_stopwords:
                word_list.append(word)
    for garbage in trash:
        word_list = [ word.replace(garbage, '') for word in word_list ]
    for garbage in trash_1:
        word_list = [ word.replace(garbage, '') for word in word_list ]

    word_list = [word for word in word_list if (word not in trash) \
                                           and (word not in trash_1) \
                                           and (word not in stopwords) \
                                           and not is_all_num(word)]
    word_list = [ word for word in word_list if word != '' ]
    return word_list


def tokenize_head(head_list, raw):
    for text in raw:
        word_list = tokenize_word(text.get_text())
        for word in word_list:
            if word not in head_list:
                head_list.append(word)
    return head_list


def wordcount(raw_data, keyword):
    head_list = []
    head_list = tokenize_head(head_list, raw_data.find_all('title'))
    head_list = tokenize_head(head_list, raw_data.find_all('h1'))
    head_list = tokenize_head(head_list, raw_data.find_all('h2'))
    head_list = tokenize_head(head_list, raw_data.find_all('h3'))
    head_list = tokenize_head(head_list, raw_data.find_all('h4'))
    head_list = tokenize_head(head_list, raw_data.find_all('h5'))
    head_list = tokenize_head(head_list, raw_data.find_all('h6'))

    for text in raw_data.findAll(text=True):
        word_list = tokenize_word(text)
        for word in word_list:
            if word not in keyword["keyword"]:
                keyword["keyword"].update({word: {"tf": 1, "idf": 0, "weight": 0, "in_head": "No"}})
                if word in head_list:
                    keyword["keyword"][word]["in_head"] = "Yes"
            else:
                keyword["keyword"][word]["tf"] += 1
            keyword["count"] += 1
    return keyword


def find_keyword(raw):
    from bs4 import BeautifulSoup
    from time import time
    import os

    start_time = time()
    path = os.path.join(os.getcwd(), "core/data/raw_html")
    raw_data = BeautifulSoup(open(os.path.join(path, raw)), 'html.parser')
    for script in raw_data(['script', 'style', '[document]', 'noscript']):
        script.extract()

    keyword = {
        "count": 0,
        "keyword":{},
    }

    keyword = wordcount(raw_data, keyword)
    for word in list(keyword["keyword"].keys()):
        if (keyword["keyword"][word]["tf"] <= 3 and keyword["keyword"][word]["in_head"] == "No") or \
           ("\\" in repr(word)):
            keyword["count"] -= keyword["keyword"][word]["tf"]
            del keyword["keyword"][word]

    # for word in keyword["keyword"]:
    #     keyword["keyword"][word]["tf"] /= keyword["count"]

    times = time() - start_time
    return times, keyword


def save_text_file(save_path, data):
    with open(save_path, 'w') as destination:
        destination.write(str(data))


def get_raw(url):
    import os
    import urllib3
    urllib3.disable_warnings()
    from uuid import uuid5, NAMESPACE_DNS
    from bs4 import BeautifulSoup

    file_name = str(uuid5(NAMESPACE_DNS, url)).replace("-", "") + ".html"
    path = os.path.join(os.getcwd(), "core/data/raw_html")
    save_path = os.path.join(path, file_name)
    # if not os.path.exists(path):
    #     os.makedirs(path)

    # if os.path.exists(save_path):
    #     return file_name

    http = urllib3.PoolManager()
    try:
        html = http.request('GET', url, timeout=5.0)
    except:
        return "error: timeout"

    if html.status != 200:
        return "error: status code"

    try:
        raw_html = html.data.decode('utf-8')
    except:
        raw_html = html.data

    raw_data = BeautifulSoup(raw_html, 'html.parser')
    for script in raw_data(['script', 'style', '[document]']):
        script.extract()

    save_text_file(save_path, raw_data)
    return file_name


def idf_updated(pf):
    from page.models import User
    from explore.models import PageFrequency as PF
    remain_list = list(PF.objects(user__in=User.objects[:10], raw__not__startswith="error", keyword__ne=None, id__ne=pf.id))
    keyword = pf.keyword
    for word in list(keyword['keyword'].keys()):
        if keyword['keyword'][word]['in_head'] == 'Yes':
            freq = 0
            for remain in remain_list:
                if word in list(remain.keyword['keyword'].keys()):
                    freq += 1
            keyword['keyword'][word]['idf'] = freq+1
    pf.keyword = keyword
    pf.save()
