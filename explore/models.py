from mongoengine import *
from django.db import models
from page.models import User, Page


class TempKeyword(Document):
    key = StringField(null=True)
    keyword_list = ListField(null=True, default=[])

    def __str__(self):
        return self.key


class TopKeyword(Document):
    date_before = DateTimeField(required=True)
    date_after = DateTimeField(required=True)
    word_list = ListField(null=True, default=[])
    types = IntField(default=0)


class StreamUser(Document):
    user_id = StringField(required=True)
    date_before = DateTimeField(required=True)
    date_after = DateTimeField(required=True)
    word_list = DictField(null=True, default={})
    types = IntField(default=0)


class Corpus(Document):
    word = StringField(required=True)
    count = IntField(default=0)


class PageDate(Document):
    date = DateTimeField(required=True)

    meta = {
        'ordering': ['date']
    }


class PageFrequency(Document):
    user = ReferenceField(User)
    date = ReferenceField(PageDate)
    domain = StringField(required=True)
    url = StringField(required=True)
    count = IntField(required=True, default=1)
    category = StringField(null=True)
    raw = StringField(null=True)
    keyword = DictField(null=True, default={})
    top_keyword = ListField(null=True, default=[])
    time = FloatField(null=True)

    def __str__(self):
        return self.url

    @staticmethod
    def pull_train():
        from .caches import cached_train_pf
        return cached_train_pf()

    @staticmethod
    def update_cached_train(pf_id):
        from .caches import update_cached_train
        update_cached_train(pf_id)


def make(inputs):
    from datetime import datetime

    user = inputs[0]
    index = inputs[1]
    print("user:", index, "start")
    date_temp = PageDate(date=datetime.now())
    date_temp.save()
    delete_date = date_temp
    pf_temp = PageFrequency(url="test", domain="domain")
    pf_temp.save()
    delete_pf = pf_temp

    page_list = list(Page.objects(user=user))
    print("length page:", len(page_list))
    for page in page_list:
        if date_temp.date != page.time_access.date():
            date = PageDate.objects(date=page.time_access.date()).first()
            if not date:
                date = PageDate(date=page.time_access.date())
                date.save()
            date_temp = date

        pf = PageFrequency.objects(user=user, date=date_temp, url=page.url).first()
        if pf:
            pf.count += 1
        else:
            pf = PageFrequency(user=user, date=date_temp, domain=page.domain,
                               url=page.url)
        pf.save()
    print("user:", index, "pass")

    delete_date.delete()
    delete_pf.delete()


def make_top_keyword(inputs):
    from mongoengine.queryset.visitor import Q
    date_before = inputs[0]
    date_after = inputs[1]
    # user_id = ['5aa0d8fe19b23728fc80125e', '5aa0d90219b23728fc801c8b', '5aa0d90219b23728fc801d57', '5aa0d90319b23728fc801f45', '5aa0d90319b23728fc801f90', '5aa0d90819b23728fc802f8e', '5aa0d90919b23728fc803140', '5aa0d90919b23728fc8031fe', '5aa0d90a19b23728fc80353e', '5aa0d90b19b23728fc803637']
    date = PageDate.objects(Q(date__gte=date_before) & Q(date__lte=date_after))
    pf_list = list(PageFrequency.objects(date__in=date))
    word_list = {}
    top_keyword = []

    for pf in pf_list:
        if pf.top_keyword:
            for word in pf.top_keyword:
                if word['word'] not in word_list:
                    word_list.update({
                        word['word']: word['weight']
                        # word['word']: pf.count
                    })
                else:
                    word_list[word['word']] += word['weight']

    for word in word_list.keys():
        top_keyword.append({
            "value": word,
            "count": round(word_list[word])
        })

    from operator import itemgetter
    top_keyword = sorted(top_keyword, key=itemgetter('count'), reverse=True)[:100]
    i = 0
    for word in top_keyword:
        word['index'] = i
        i += 1

    new_top = TopKeyword.objects(date_before=date_before, date_after=date_after, types=2).first()
    if new_top:
        new_top.word_list = top_keyword
    else:
        new_top = TopKeyword(
            word_list=top_keyword,
            date_before=date_before,
            date_after=date_after,
            types=2
        )
    new_top.save()
