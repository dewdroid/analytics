from django.core.cache import cache
from page.models import User
from .models import PageDate as PD, PageFrequency as PF
from mongoengine.queryset.visitor import Q
from page.models import User

def get_time_out_day():
    return 24 * 60 * 60 * 365


def cached_category(before, after):
    key = "category" + before + after
    if cache.has_key(key):
        return cache.get(key)

    # user_id = ['5aa0d8fe19b23728fc80125e', '5aa0d90219b23728fc801c8b', '5aa0d90219b23728fc801d57', '5aa0d90319b23728fc801f45', '5aa0d90319b23728fc801f90', '5aa0d90819b23728fc802f8e', '5aa0d90919b23728fc803140', '5aa0d90919b23728fc8031fe', '5aa0d90a19b23728fc80353e', '5aa0d90b19b23728fc803637']
    date_list = PD.objects(Q(date__gte=before) & Q(date__lte=after))

    sum_know = PF.objects(date__in=date_list, category__startswith="T").count()
    sum_gen = PF.objects(date__in=date_list, category__startswith="N").count()

    response = [
        {
            "name": "Knowledge",
            "value": round((sum_know/(sum_know+sum_gen))*100, 2)
        },
        {
            "name": "General",
            "value": round((sum_gen/(sum_know+sum_gen))*100, 2)
        }
    ]
    cache.set(key, response, get_time_out_day())
    return response



def cached_train_pf():
    key = 'train_data'
    if cache.has_key(key):
        train_data = cache.get(key)
        rand = choice(train_data)
        result = cache.get(rand)
    else:
        result = None

    if result is None:
        try:
            user_all = User.objects
            user_list = []
            for i in [21, 51, 53, 59, 61, 87, 91, 92, 95, 97]:
                user_list.append(user_all[i])
            train_data = []
            for pf in list(PF.objects(user__in=user_list, category=None)):
                cache.set(str(pf.id), pf, get_time_out_day())
                train_data.append(str(pf.id))
            cache.set(key, train_data, get_time_out_day())

            rand = choice(train_data)
            result = cache.get(rand)
        except:
            print("error")
            result = -1
        return (None, 0) if result == -1 else (result, len(train_data))
    else:
        return result, len(train_data)


def update_cached_train(pf_id):
    key = 'train_data'
    if cache.has_key(key):
        result = cache.get(key)
        if result:
            result.remove(pf_id)
            cache.delete(key)
            cache.set(key, result, get_time_out_day())
            if cache.has_key(pf_id):
                cache.delete(pf_id)
