from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route

from page.models import Page, User, Dummy
from django.core.cache import cache
from .serializers import MakeCategorySerializer, KeywordSerializer, TextSerializer
from .models import PageDate as PD, PageFrequency as PF, Corpus, TopKeyword as TK, TempKeyword
from mongoengine.queryset.visitor import Q
from uuid import uuid5, NAMESPACE_DNS


class PDViewSet(mixins.ListModelMixin,
                viewsets.GenericViewSet):
    queryset = Dummy.objects.all()

    def list(self, request):
        response = []
        for date in list(PD.objects):
            response.append(date.date.date())
        return Response(response)


class KeywordViewSet(mixins.CreateModelMixin,
                     viewsets.GenericViewSet):
    queryset = Dummy.objects.all()

    action_serializers = {
        'create': KeywordSerializer,
        'keyword_user': KeywordSerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        mode = cache.get("mode")
        key = str(mode) + "keyword" + serializer.data['keyword'] + serializer.data['before'] + serializer.data['after']
        key = str(uuid5(NAMESPACE_DNS, key)).replace("-", "")
        response = cache.get(key)
        if response:
            return Response(response)

        date_list = PD.objects(Q(date__gte=serializer.data['before']) &
                               Q(date__lte=serializer.data['after']))

        # user_id = ['5aa0d8fe19b23728fc80125e', '5aa0d90219b23728fc801c8b', '5aa0d90219b23728fc801d57', '5aa0d90319b23728fc801f45', '5aa0d90319b23728fc801f90', '5aa0d90819b23728fc802f8e', '5aa0d90919b23728fc803140', '5aa0d90919b23728fc8031fe', '5aa0d90a19b23728fc80353e', '5aa0d90b19b23728fc803637']

        target = serializer.data['keyword']
        response = []
        for date in date_list:
            obj = {
                "date": date.date.date(),
                "frequency": 0
            }
            if mode == 1:
                pf_list = list(PF.objects(date=date, category__startswith="T"))
            else:
                pf_list = list(PF.objects(date=date))
            for pf in pf_list:
                if pf.top_keyword:
                    for word in pf.top_keyword:
                        if target == word['word']:
                            obj['frequency'] += pf.count
                            break
            response.append(obj)
        cache.set(key, response, 60*60)
        return Response(response)

    @list_route(methods=['post'], url_path='user')
    def keyword_user(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # user_id = [[0,11], [12,42], [43,78], [79,103]]
        # user_all = []
        # for i in user_id:
        #     user_all.extend(User.objects[i[0]:i[1]])
        # response =
        mode = cache.get("mode")
        key = str(mode) + "user" + serializer.data['keyword'] + serializer.data['before'] + serializer.data['after']
        key = str(uuid5(NAMESPACE_DNS, key)).replace("-", "")
        response = cache.get(key)
        if response:
            return Response(response)

        # user_id = ['5aa0d8fe19b23728fc80125e', '5aa0d90219b23728fc801c8b', '5aa0d90219b23728fc801d57', '5aa0d90319b23728fc801f45', '5aa0d90319b23728fc801f90', '5aa0d90819b23728fc802f8e', '5aa0d90919b23728fc803140', '5aa0d90919b23728fc8031fe', '5aa0d90a19b23728fc80353e', '5aa0d90b19b23728fc803637']
        user_id = [[0,11], [12,42], [43,78], [79,103]]
        user_list = []
        for i in user_id:
            user_list.extend(User.objects[i[0]:i[1]])

        date_list = PD.objects(Q(date__gte=serializer.data['before']) &
                               Q(date__lte=serializer.data['after']))

        response = []
        target = serializer.data['keyword']
        for user in user_list:
            obj = {
                "id": str(user.id),
                "name": user.show_name,
                "frequency": 0
            }
            if mode == 1:
                pf_list = list(PF.objects(user=user, date__in=date_list, category__startswith="T"))
            else:
                pf_list = list(PF.objects(user=user, date__in=date_list))

            for pf in pf_list:
                if pf.top_keyword:
                    for word in pf.top_keyword:
                        if target == word['word']:
                            obj['frequency'] += pf.count
                            break
            response.append(obj)

        response = [ obj for obj in response if obj['frequency'] > 0 ]
        from operator import itemgetter
        response = sorted(response, key=itemgetter('frequency'), reverse=True)
        cache.set(key, response, 60*60)
        return Response(response)


class PFViewSet(viewsets.GenericViewSet):
    queryset = Dummy.objects.all()

    action_serializers = {
        'make_category': MakeCategorySerializer,
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    @detail_route(methods=['get'], url_path='change-mode')
    def change_mode(self, request, pk=None):
        if int(pk) in [1,2]:
            cache.set("mode", int(pk), 24 * 60 * 60 * 365)
            return Response({"message": "success"})
        return Response({"message": "error"})

    @list_route(methods=['get'], url_path='top-keyword')
    def top_keyword(self, request, pk=None):
        from random import randint
        date_before = request.GET.get('before', '')
        date_after = request.GET.get('after', '')
        if (not date_before) and (not date_after):
            tk = TK.objects(date_before="2017-11-16", date_after="2018-01-15", types=cache.get("mode")).first()
        else:
            tk = TK.objects(date_before=date_before, date_after=date_after, types=cache.get("mode")).first()
        # word_list = sorted(tk_list.word_list, key=itemgetter('weight'), reverse=True)
        word_list = tk.word_list
        return Response(word_list)

    # @list_route(methods=['get'], url_path='show-category')
    # def show_category(self, request):
    #     pf, length = PF.pull_train()
    #     if length == 0:
    #         return Response({"message": "That's enough. Thank you !!"})
    #
    #     response = {
    #         "page_id": str(pf.id),
    #         "page": pf.url,
    #         "remain": length
    #     }
    #     return Response(response)
    #
    # @list_route(methods=['post'], url_path='make-category')
    # def make_category(self, request):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     pf = PF.objects(id=serializer.data['page_id']).first()
    #     if pf:
    #         pf.category = "Technology" if serializer.data['is_technology'] == 1 else "Non-Technology"
    #         pf.save()
    #         pf.update_cached_train(str(pf.id))
    #     return Response(status=status.HTTP_200_OK)

    @list_route(methods=['get'], url_path='category-proportion')
    def category_proportion(self, request):
        before = request.GET.get('before', '')
        after = request.GET.get('after', '')
        from .caches import cached_category
        return Response(cached_category(before, after))

    # @list_route(methods=['get'], url_path='make-pf')
    # def make_pf(self, request):
    #     from page.models import User
    #     from explore.models import make
    #     queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=31536000)
    #     i = 0
    #     for user in list(User.objects[0:11]):
    #         queue.enqueue(make, [user, i])
    #         i += 1
    #
    #     i = 12
    #     for user in list(User.objects[12:42]):
    #         queue.enqueue(make, [user, i])
    #         i += 1
    #
    #     i = 43
    #     for user in list(User.objects[43:78]):
    #         queue.enqueue(make, [user, i])
    #         i += 1
    #
    #     i = 79
    #     for user in list(User.objects[79:103]):
    #         queue.enqueue(make, [user, i])
    #         i += 1
    #     return Response(status=status.HTTP_200_OK)

    # @list_route(methods=['get'], url_path='get-raw-html')
    # def get_raw(self, request):
    #     queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=31536000)
    #     queue.enqueue(get_raw_html)
    #     return Response(status=status.HTTP_200_OK)
    #
    # @list_route(methods=['get'], url_path='extract-keyword')
    # def extract_keyword(self, request):
    #     queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=31536000)
    #     user_list = [[0,11], [12,42], [43,78], [79,103]]
    #     index = 1
    #     for i in user_list:
    #         for user in list(User.objects[i[0]:i[1]]):
    #             queue.enqueue(find_keywords, [index, user])
    #             index += 1
    #     return Response(status=status.HTTP_200_OK)


def clean_word(inputs):
    pf = inputs[0]
    corpus_list = inputs[1]
    keyword = pf.keyword
    for word in corpus_list:
        if word in list(keyword['keyword'].keys()):
            keyword['count'] -= keyword['keyword'][word]['tf']
            del keyword['keyword'][word]
    pf.keyword = keyword
    pf.save()


def duplicated(pf):
    from core.extract_keywords import find_keyword
    time, keyword = find_keyword(pf.raw)
    new_pf = PF(
        user=pf.user,
        date=pf.date,
        domain=pf.domain,
        url=pf.url,
        count=pf.count,
        category=pf.category,
        raw=pf.raw,
        time=time,
        keyword=keyword
    )
    new_pf.save()
    pf.delete()


def page_top_keyword(pf):
    keyword = pf.keyword['keyword']
    keyword_list = sorted(keyword.items(), key=lambda x:x[1]['weight'], reverse=True)
    top_list = []
    for word in keyword_list:
        if word[1]['in_head'] == "Yes" and word[1]['weight'] > 1:
            top_list.append({
                "word": word[0],
                "weight": word[1]['weight'],
            })
    if not top_list:
        top_list.append({
                "word": "not-have-word",
                "weight": 0,
            })
    pf.top_keyword = top_list
    pf.save()


def smooth_idf(pf):
    from math import log10
    keyword = pf.keyword
    word_list = list(keyword['keyword'].keys())
    max_count = 0
    for word in word_list:
        if keyword['keyword'][word]['tf'] > max_count:
            max_count = keyword['keyword'][word]['tf']

    for word in word_list:
        tf = 0.5 + (0.5*(keyword['keyword'][word]['tf']/max_count))
        idf = log10(18981/keyword['keyword'][word]['idf'])
        keyword['keyword'][word]['weight'] = tf * idf
    pf.keyword = keyword
    pf.save()


def make_temp_keyword(url):
    from uuid import uuid5, NAMESPACE_DNS
    pf = PF.objects(url=url).first()
    keyword_list = list(pf.keyword['keyword'].keys())
    key = str(uuid5(NAMESPACE_DNS, pf.url)).replace("-", "")
    TempKeyword(key=key, keyword_list=keyword_list).save()


def make_idf(url):
    pf_list = list(PF.objects(url=url))
    keyword = pf_list[0].keyword

    word_list = []
    for word in list(keyword['keyword'].keys()):
        if keyword['keyword'][word]['in_head'] == 'Yes':
            corpus = Corpus.objects(word=word).first()
            if corpus:
                keyword['keyword'][word]['idf'] = corpus.count
            else:
                word_list.append({word: 1})

    if word_list:
        from uuid import uuid5, NAMESPACE_DNS
        key = str(uuid5(NAMESPACE_DNS, url)).replace("-", "")
        remain_list = list(TempKeyword.objects(key__ne=key))

        for remain in remain_list:
            keyword_list = list(remain.keyword_list)
            for obj in word_list:
                word = list(obj.keys())[0]
                if word in keyword_list:
                    obj[word] += 1

        for obj in word_list:
            word = list(obj.keys())[0]
            Corpus(word=word, count=obj[word]).save()
            keyword['keyword'][word]['idf'] = obj[word]

    for pf in pf_list:
        pf.keyword = keyword
        pf.save()


def find_keywords(pf):
    from core.extract_keywords import find_keyword
    times, keyword = find_keyword(pf.raw)
    pf.time = times
    pf.keyword = keyword
    error = 1
    try:
        pf.save()
    except:
        new_pf = PF(
            user=pf.user,
            date=pf.date,
            domain=pf.domain,
            url=pf.url,
            count=pf.count,
            category=pf.category,
            raw=pf.raw,
            keyword=keyword,
            time=times
        )
        new_pf.save()
        pf.delete()
        print("error :", error)
        error += 1


def find_keywordss(pf):
    from core.extract_keywords import find_keyword
    temp = PF.objects(url=pf.url, keyword__ne=None).first()
    if temp:
        pf.time = temp.time
        pf.keyword = temp.keyword
    else:
        times, keyword = find_keyword(pf.raw)
        pf.time = times
        pf.keyword = keyword
    pf.save()


def get_raw_html(pf):
    from core.extract_keywords import get_raw
    raw = get_raw(pf.url)
    pf.raw = raw
    pf.save()
