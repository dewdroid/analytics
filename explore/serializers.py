from rest_framework import serializers


class MakeCategorySerializer(serializers.Serializer):

    page_id = serializers.CharField(required=True)
    is_technology = serializers.IntegerField(required=True)


class KeywordSerializer(serializers.Serializer):

    keyword = serializers.CharField(required=True)
    before = serializers.CharField(required=True)
    after = serializers.CharField(required=True)


class TextSerializer(serializers.Serializer):

    word = serializers.CharField(required=True)
