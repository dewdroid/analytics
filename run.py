import os
import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'project.settings'
django.setup()
import django_rq
from explore.models import PageFrequency as PF, PageDate as PD
from explore.views import page_top_keyword
from page.models import User, Page
import pandas as pd
import numpy as np
from mongoengine import *
connect('analytics')

pf_list = list(PF.objects(category=None, top_keyword=None))
queue = django_rq.get_queue('default', autocommit=True, async=True, default_timeout=31536000)
for pf in pf_list:
    queue.enqueue(page_top_keyword, pf)
